FROM hypriot/rpi-python  
MAINTAINER Damon Jebb "damon.jebb@gmail.com"  
      
RUN apt-get -y update  
RUN apt-get -y install openssh-server supervisor openjdk-7-jdk git
      
RUN echo "root:password" | chpasswd  
RUN useradd jenkins  
RUN echo "jenkins:jenkins" | chpasswd  
      
RUN mkdir -p /var/run/sshd  
RUN echo y | ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''  
RUN sed -i 's|session    required     pam_loginuid.so|session    optional     pam_loginuid.so|g' /etc/pam.d/sshd  
     
RUN mkdir -p /var/run/supervisord  
ADD supervisord.conf /etc/supervisord.conf  
      
RUN mkdir -p /home/jenkins 
RUN chown jenkins /home/jenkins
USER jenkins
RUN mkdir /home/jenkins/.ssh
RUN chmod 700 /home/jenkins/.ssh
RUN echo "Host *\n\tStrictHostKeyChecking no" >> /home/jenkins/.ssh/config
USER root
RUN pip install pytest
EXPOSE 22  
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]  
